package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;

    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // Implementation of Rock Paper Scissors

        while (true) {
            System.out.println("Let's play round " + roundCounter);

            String userInput = userChoice();
            String computerInput = computerChoice();

            System.out.printf("Human chose %s, computer chose %s. ", userInput, computerInput);

            // Declare winner

            if (rockPaperScissorsResults(userInput, computerInput)) {
                computerScore += 1;
                System.out.println("Computer wins!");
            }
            else if (rockPaperScissorsResults(computerInput, userInput)) {
                humanScore += 1;
                System.out.println("Human wins!");
            }
            else {
                System.out.println("It's a tie!");
            }
            
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
                
            // Ask the player if they want to continue
            if (continuePlaying()) {
                continue;
            }
            else {
                break;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // Obtain a random number between [0 - range-1].
    public int randomInteger(int range) {
        Random random_number = new Random();
        int number = random_number.nextInt(range);
        return number;
    }

    // Return results of the rock/paper/scissors match
    public Boolean rockPaperScissorsResults(String input_1, String input_2) {
        if (input_1.equals("rock")) {
            return (input_2.equals("paper"));
        }
        else if (input_1.equals("paper")) {
            return (input_2.equals("scissors"));
        }
        else {
            return (input_2.equals("rock"));
        }
    }

    // Check if the player wants to continue
    public boolean continuePlaying() {
        String userInput = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            
        if (userInput.equals("y")) {
            roundCounter += 1;
            return true;
        }

        else {
            System.out.println("Bye bye :)");
            return false;
        }  
    }

    // Validate that the user choice is in the list of available choices

    public boolean validateChoice(String userChoice, List<String> choices) {
        return choices.contains(userChoice);
    }

    // Check if the user input matches the available choices in rpsChoices
    public String userChoice() {
        while (true) {
            String userInput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if (rpsChoices.contains(userInput)){
                return userInput;
            }
            else {
                System.out.printf("I do not understand %s. Could you try again?\n", userInput);
            }
        }
    }

    // Generate the computer's choice
    public String computerChoice() {
        return rpsChoices.get(randomInteger(3));
    }

}